These scripts generate input files for defining a proper apaerture in MADX. The resulting sequence and MADX files can be called directly by any MAD-X script using HLLHCV1.5, and is compatible with both Thick and Thin optics.

originalFiles/ contains the input files for the aperture generation:

-- allapert.b1 / allapert.b2 are Twiss files produced for HLLHCV1.0 and has a detailed definition of the aperture in the arcs. These files are likely not to change.
-- APERTURE_LHC_LS3_*.seq are sequence files compatible with MADX downloaded from the layout database. These files require a lot of patching, which is done by the bash scripts.


The generated files are output in processedFiles/
Some of these are simply manual patches and are just copied as is from originalFiles/manualPatches/

extraTools/ contains various tools:
-- scanForNegDrifts/ contains files for repeatedly running MADX with the generated layoutDB file, but uncommenting the elements that cause negative drifts, one at a time, to cross-check that they are really negative
-- compareLayoutDB.sh compares two layoutDB files
-- layoutDB_b2tob4.sh converts a layoutDB files from the layout database such that it is compatible with b4, instead of b2. This means that all s positions are inverted. This also splits the layoutDB file into two parts.
-- parseExistingApertureModelThick.py is called by the main scripts to pick out markers from the existing HLLHCV1.0 twiss files.

patchesFiles/ contains all the input files for the bash scripts modifying the layoutDB files
-- layoutDB_negDrifts: list of elements causing negative drift errors, these are commented
-- patches: list of elements causing suspicious apertures, these are commented
-- shiftLeft: list of elements that should be shifted slightly to the left (smaller s) since their s position overlaps with another element of a different aperture size, causing wedges in the aperture.
-- shiftRight: same as shiftLeft, but shift in opposite s direction

madx/ contains two sample madx scripts that call the full aperture and generate twiss files for beam 1, 2 and 4. Note that the beam2 aperture is not complete, please use beam4 instead.

madx/backup/ contains the relevant madx files that are taken from /afs/cern.ch/eng/lhc/optics/ in case the official files change.
