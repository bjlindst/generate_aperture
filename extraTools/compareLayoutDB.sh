#!/bin/bash

cp back/APERTURE_LHC_LS3_28-JUL-2021.seq temp/fileNew.seq
cp back/APERTURE_LHC_LS3_31-MAR-2021.seq temp/fileOld.seq
sed -i 's/\*//g' temp/fileNew.seq
sed -i 's/\*//g' temp/fileOld.seq



lineMax=$(wc -l temp/fileOld.seq)
lineMax2=$(wc -l temp/fileOld.seq | tr -dc ̈́'0-9')
echo "lineMax = $lineMax"            
echo "lineMax2 = $lineMax2"
hej=$((lineMax2+1))

echo "hej = $hej"

for ((line=1; line<=$lineMax2; line++))
do
    echo "line = $line"
    line2=$line"p"
    #echo $line2
    #sed -n $line2 temp/fileOld.seq
    line3=$(sed -n $line2 temp/fileOld.seq)
    #echo $hej2     
    [[ ! -z "${line3// }" ]] && echo $line3 && sed -i 's/'"$line3"'/removed/g' temp/fileOld.seq && sed -i 's/'"$line3"'/removed/g' temp/fileNew.seq
done

