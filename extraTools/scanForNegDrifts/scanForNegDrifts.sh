#rm out.txt
for line in {202..209}
do
    sed "$line s/./#&/" layoutDB_negDrifts.txt > layoutDB_negDrifts_mod.txt
    echo $line >> out.txt
    sed -n "$line p" layoutDB_negDrifts_mod.txt >> out.txt
    ./sedScript.sh
    ./madx job_sample_thin.madx > madxOut.txt 2>&1
    tail -n 1 madxOut.txt >> out.txt
done
