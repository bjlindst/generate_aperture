#rm out.txt
for line in {1..157}
do
    sed "$line s/./#&/" layoutDB_negDrifts_b4_part2.txt > layoutDB_negDrifts_mod.txt
    echo $line >> out.txt
    sed -n "$line p" layoutDB_negDrifts_mod.txt >> out.txt
    ./sedScript_b4.sh
    ./madx job_sample_thin_b4.madx > madxOut.txt 2>&1
    tail -n 1 madxOut.txt >> out.txt
done
