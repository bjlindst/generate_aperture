'''
B.Lindstrom, 2022-01-24 (last version)
updated file to be consistent with v1.5 and layoutDB
A.Mereghetti, 2015-10-16 (prev version)
file for grepping aperture markers in existing
   aperture models (thick lens) and dump them
   in MADX files, ready to be used;
the script allows to select markers only in certain
   regions of the accelerator, and to handle some
   exceptions.
for the time being: B1/B4 are supported, but not B2.

note on usage:
python parseExistingApertureModelThick.py <filename> <beamID>
'''

import sys

if ( __name__ == '__main__' ):
    if ( len( sys.argv ) < 3 ):
        print(' note on usage: ')
        print(sys.argv[0],' <fileName> <beamID> ')
        print(' aborting...')
        sys.exit()
    # user settings (most frequently changed):
    iFileName = sys.argv[1]
    idBeam = int( sys.argv[2] ) # 1: B1; 2: B2;
    isB4 = False
    if ( idBeam == 2 ):
        isB4 = True

    # other settings:
    oFileNameDef = 'fromCollimation_define.madx'
    oFileNameIns = 'fromCollimation_install.madx'
    AccelLen = 26658.88320000 # [m]
    MARKERlineFmt1 = '%s %-20s: MARKER, APERTYPE=RECTELLIPSE, APERTURE={%10s,%10s,%10s,%10s}; \n'
    MARKERlineFmt2 = '%s install, element=%-20s, at=%-20s; \n'

    #
    if ( idBeam == 1 ):
        # look for aperture markers in this range
        #             from           to
        eleNames = [ "S.DS.R1.B1" , "MK.IP2.L.29", 
                     "MK.IP2.R.26", "E.DS.L5.B1" ,
                     "S.DS.R5.B1" , "MK.IP8.L.28",
                     "MK.IP8.R.27", "E.DS.L1.B1" ]
        # look for aperture markers assigned to drifts in this range
        #    from          to
        eleNamesDrift = [
#            "E.DS.L7.B1", "S.DS.R7.B1"
            ]
        # do not dump these elements
        escapeFromDumping = [ \
            "LHCINJ.B1", "IR2$START" 
            ]
        # dump elements but commented, since, if they are inserted, 
        #      MADX will generate a negative drift error
        negativeDrift = [ 
            'VSSB.S.C11R1.B1', 'VSSB.E.C11R1.B1',
            'VSSB.S.C11L2.B1', 'VSSB.E.C11L2.B1',
            'VSSB.S.C11R2.B1', 'VSSB.E.C11R2.B1',
            'VSSB.S.C11L3.B1', 'VSSB.E.C11L3.B1',
            'VSSB.S.C11R3.B1', 'VSSB.E.C11R3.B1',
            'VSSB.S.C11L4.B1', 'VSSB.E.C11L4.B1',
            'VSSB.S.C11R4.B1', 'VSSB.E.C11R4.B1',
            'VSSB.S.C11L5.B1', 'VSSB.E.C11L5.B1',
            'VSSB.S.C11R5.B1', 'VSSB.E.C11R5.B1',
            'VSSB.S.C11L6.B1', 'VSSB.E.C11L6.B1',
            'VSSB.S.C11R6.B1', 'VSSB.E.C11R6.B1',
            'VSSB.S.C11L7.B1', 'VSSB.E.C11L7.B1',
            'VSSB.S.C11R7.B1', 'VSSB.E.C11R7.B1',
            'VSSB.S.C11L8.B1', 'VSSB.E.C11L8.B1',
            'VSSB.S.C11R8.B1', 'VSSB.E.C11R8.B1',
            'VSSB.S.C11L1.B1', 'VSSB.E.C11L1.B1',
            'VSSG.S.7L2.B1'  , 'VSSG.E.7L2.B1'  ,
            'VSSK.S.3L2.B1'  , 'VSSK.E.3L2.B1'  ,
            'VSSK.S.3R2.B1'  , 'VSSK.E.3R2.B1'  ,
            'VSSG.S.7R2.B1'  , 'VSSG.E.7R2.B1'  ,
            'VSSG.S.7L3.B1'  , 'VSSG.E.7L3.B1'  ,
            'VSSK.S.3L3.B1'  , 'VSSK.E.3L3.B1'  ,
            'VSSK.S.3R3.B1'  , 'VSSK.E.3R3.B1'  ,
            'VSSG.S.7R3.B1'  , 'VSSG.E.7R3.B1'  ,
            'VSSG.S.7L4.B1'  , 'VSSG.E.7L4.B1'  ,
            'VSSK.S.3L4.B1'  , 'VSSK.E.3L4.B1'  ,
            'VSSK.S.3R4.B1'  , 'VSSK.E.3R4.B1'  ,
            'VSSG.S.7R4.B1'  , 'VSSG.E.7R4.B1'  ,
            'VSSG.S.7L5.B1'  , 'VSSG.E.7L5.B1'  ,
            'VSSK.S.3L5.B1'  , 'VSSK.E.3L5.B1'  ,
            'VSSK.S.3R5.B1'  , 'VSSK.E.3R5.B1'  ,
            'VSSG.S.7R5.B1'  , 'VSSG.E.7R5.B1'  ,
            'VSSG.S.7L6.B1'  , 'VSSG.E.7L6.B1'  ,
            'VSSK.S.3L6.B1'  , 'VSSK.E.3L6.B1'  ,
            'VSSK.S.3R6.B1'  , 'VSSK.E.3R6.B1'  ,
            'VSSG.S.7R6.B1'  , 'VSSG.E.7R6.B1'  ,
            'VSSG.S.7L7.B1'  , 'VSSG.E.7L7.B1'  ,
            'VSSK.S.3L7.B1'  , 'VSSK.E.3L7.B1'  ,
            'VSSK.S.3R7.B1'  , 'VSSK.E.3R7.B1'  ,
            'VSSG.S.7R7.B1'  , 'VSSG.E.7R7.B1'  ,
            'VSSG.S.7L8.B1'  , 'VSSG.E.7L8.B1'  ,
            'VSSK.S.3L8.B1'  , 'VSSK.E.3L8.B1'  ,
            'VSSK.S.3R8.B1'  , 'VSSK.E.3R8.B1'  ,
            'VSSG.S.7R8.B1'  , 'VSSG.E.7R8.B1'  ,
            'VSSG.S.7L1.B1'  , 'VSSG.E.7L1.B1'  ,
            'VSSK.S.3L1.B1'  , 'VSSK.E.3L1.B1'  ,
            'VSSK.S.3R1.B1'  , 'VSSK.E.3R1.B1'  ,
            'VSSG.S.7R1.B1'  , 'VSSG.E.7R1.B1'  ,
            'BPMWE.S.5L3.B1' , 'BPMWE.E.5L3.B1' ,
            'VSSG.S.B5L6.B1' , 'VSSG.E.B5L6.B1' ,
            'BPMYA.S.5L6.B1' , 'BPMYA.E.5L6.B1' ,
            'VSSG.S.A5L6.B1' , 'VSSG.E.A5L6.B1' ,
            'VSSG.S.A5R6.B1' , 'VSSG.E.A5R6.B1' ,
            'VSSG.S.B5R6.B1' , 'VSSG.E.B5R6.B1' ,
            'BPMWB.E.4L8.B1' , 'BPMWB.S.4L8.B1' , # bjorn added
            ]
        # dump elements but commented, since, if they are inserted, 
        #      MADX will complain due to their position
        incorrectPositioning = [ \
            'BPMSA.S.B4L6.B1', 'BPMSA.E.B4L6.B1',
            'BPMSA.S.A4L6.B1', 'BPMSA.E.A4L6.B1',
            'BPMSD.S.4R6.B1' , 'BPMSD.E.4R6.B1' ,
            'BPMSB.S.A4R6.B1', 'BPMSB.E.A4R6.B1',
            'BPMSB.S.B4R6.B1', 'BPMSB.E.B4R6.B1',
            'BPMWE.S.5R3.B1' , 'BPMWE.E.5R3.B1' 
            ]
        duplicateElements = [ \
            'BPM.S.7L2.B1', 'BPM.E.7L2.B1',
            'VSSB.S.7L2.B1', 'VSSB.E.7L2.B1',
            'VSSB.S.6L2.B1', 'VSSB.E.6L2.B1',
            'BPMR.S.6L2.B1', 'BPMR.E.6L2.B1',
            'VSSG.S.5L2.B1', 'VSSG.E.5L2.B1',
            'BPMYB.S.5L2.B1', 'BPMYB.E.5L2.B1',
            'BPMYB.S.4L2.B1', 'BPMYB.E.4L2.B1',
            'VSSG.S.4L2.B1', 'VSSG.E.4L2.B1',
            'VSSJ.S.4L2.B1', 'VSSJ.E.4L2.B1',
            'BPMWB.S.4L2.B1', 'BPMWB.E.4L2.B1',
            'BPMSX.S.4L2.B1', 'BPMSX.E.4L2.B1',
            'VSSK.S.4L2.B1', 'VSSK.E.4L2.B1',
            'VSSG.S.B3L2.B1', 'VSSG.E.B3L2.B1',
            'VSSG.S.A3L2.B1', 'VSSG.E.A3L2.B1',
            'BPMS.S.2L2.B1', 'BPMS.E.2L2.B1',
            'VSSL.S.2L2.B1', 'VSSL.E.2L2.B1',
            'BPMSW.S.1L2.B1', 'BPMSW.E.1L2.B1',
            'BPMSW.S.1R2.B1', 'BPMSW.E.1R2.B1',
            'VSSL.S.1R2.B1', 'VSSL.E.1R2.B1',
            'BPMS.S.2R2.B1', 'BPMS.E.2R2.B1',
            'VSSG.S.2R2.B1', 'VSSG.E.2R2.B1',
            'VSSG.S.3R2.B1', 'VSSG.E.3R2.B1',
            'VSSK.S.4R2.B1', 'VSSK.E.4R2.B1',
            'BPMSX.S.4R2.B1', 'BPMSX.E.4R2.B1',
            'BPMWB.S.4R2.B1', 'BPMWB.E.4R2.B1',
            'VSSJ.S.4R2.B1', 'VSSJ.E.4R2.B1',
            'VSSG.S.4R2.B1', 'VSSG.E.4R2.B1',
            'BPMYB.S.4R2.B1', 'BPMYB.E.4R2.B1',
            'VSSB.S.5R2.B1', 'VSSB.E.5R2.B1',
            'BPMR.S.5R2.B1', 'BPMR.E.5R2.B1',
            'VSSB.S.6R2.B1', 'VSSB.E.6R2.B1',
            'BPM.S.6R2.B1', 'BPM.E.6R2.B1',
            'BPM_A.S.7R2.B1', 'BPM_A.E.7R2.B1',
            'VSSB.S.7R2.B1', 'VSSB.E.7R2.B1',
            'BPM.S.7L3.B1', 'BPM.E.7L3.B1',
            'VSSB.S.7L3.B1', 'VSSB.E.7L3.B1',
            'VSSB.S.6L3.B1', 'VSSB.E.6L3.B1',
            'BPM.S.6L3.B1', 'BPM.E.6L3.B1',
            'BPMW.S.5L3.B1', 'BPMW.E.5L3.B1',
            'BPMWE.S.4L3.B1', 'BPMWE.E.4L3.B1',
            'BPMW.S.4L3.B1', 'BPMW.E.4L3.B1',
            'BPMW.S.4R3.B1', 'BPMW.E.4R3.B1',
            'BPMWE.S.4R3.B1', 'BPMWE.E.4R3.B1',
            'BPMW.S.5R3.B1', 'BPMW.E.5R3.B1',
            'BPMWC.S.6R3.B1', 'BPMWC.E.6R3.B1',
            'VSSB.S.6R3.B1', 'VSSB.E.6R3.B1',
            'BPMR.S.6R3.B1', 'BPMR.E.6R3.B1',
            'BPM_A.S.7R3.B1', 'BPM_A.E.7R3.B1',
            'VSSB.S.7R3.B1', 'VSSB.E.7R3.B1',
            'BPMC.S.7L4.B1', 'BPMC.E.7L4.B1',
            'VSSB.S.7L4.B1', 'VSSB.E.7L4.B1',
            'BPMYB.S.6L4.B1', 'BPMYB.E.6L4.B1',
            'VSSG.S.6L4.B1', 'VSSG.E.6L4.B1',
            'BPMYA.S.5L4.B1', 'BPMYA.E.5L4.B1',
            'VSSG.S.5L4.B1', 'VSSG.E.5L4.B1',
            'VSSJ.S.B5L4.B1', 'VSSJ.E.B5L4.B1',
            'VSSJ.S.A5L4.B1', 'VSSJ.E.A5L4.B1',
            'BPMWA.S.B5L4.B1', 'BPMWA.E.B5L4.B1',
            'BPMWA.S.A5L4.B1', 'BPMWA.E.A5L4.B1',
            'BPMWA.S.A5R4.B1', 'BPMWA.E.A5R4.B1',
            'BPMWA.S.B5R4.B1', 'BPMWA.E.B5R4.B1',
            'VSSJ.S.A5R4.B1', 'VSSJ.E.A5R4.B1',
            'VSSJ.S.B5R4.B1', 'VSSJ.E.B5R4.B1',
            'VSSG.S.5R4.B1', 'VSSG.E.5R4.B1',
            'BPMYB.S.5R4.B1', 'BPMYB.E.5R4.B1',
            'BPMYA.S.6R4.B1', 'BPMYA.E.6R4.B1',
            'VSSG.S.6R4.B1', 'VSSG.E.6R4.B1',
            'BPMCA.S.7R4.B1', 'BPMCA.E.7R4.B1',
            'VSSB.S.7R4.B1', 'VSSB.E.7R4.B1',
            'BPMYB.S.4L6.B1', 'BPMYB.E.4L6.B1',
            'VSSG.S.4L6.B1', 'VSSG.E.4L6.B1',
            'BPMSE.S.4L6.B1', 'BPMSE.E.4L6.B1',
            'BPMYA.S.4R6.B1', 'BPMYA.E.4R6.B1',
            'VSSG.S.4R6.B1', 'VSSG.E.4R6.B1',
            'BPMYB.S.5R6.B1', 'BPMYB.E.5R6.B1',
            'BPM.S.7L7.B1', 'BPM.E.7L7.B1',
            'VSSB.S.7L7.B1', 'VSSB.E.7L7.B1',
            'BPM.S.6L7.B1', 'BPM.E.6L7.B1',
            'VSSB.S.6L7.B1', 'VSSB.E.6L7.B1',
            'BPMWC.S.6L7.B1', 'BPMWC.E.6L7.B1',
            'BPMWE.S.5L7.B1', 'BPMWE.E.5L7.B1',
            'BPMW.S.5L7.B1', 'BPMW.E.5L7.B1',
            'BPMWE.S.4L7.B1', 'BPMWE.E.4L7.B1',
            'BPMW.S.4L7.B1', 'BPMW.E.4L7.B1',
            'BPMW.S.4R7.B1', 'BPMW.E.4R7.B1',
            'BPMWE.S.4R7.B1', 'BPMWE.E.4R7.B1',
            'BPMW.S.5R7.B1', 'BPMW.E.5R7.B1',
            'BPMWE.S.5R7.B1', 'BPMWE.E.5R7.B1',
            'BPMR.S.6R7.B1', 'BPMR.E.6R7.B1',
            'VSSB.S.6R7.B1', 'VSSB.E.6R7.B1',
            'BPM_A.S.7R7.B1', 'BPM_A.E.7R7.B1',
            'VSSB.S.7R7.B1', 'VSSB.E.7R7.B1',
            'BPM.S.7L8.B1', 'BPM.E.7L8.B1',
            'VSSB.S.7L8.B1', 'VSSB.E.7L8.B1',
            'VSSB.S.6L8.B1', 'VSSB.E.6L8.B1',
            'BPMR.S.6L8.B1', 'BPMR.E.6L8.B1',
            'VSSB.S.5L8.B1', 'VSSB.E.5L8.B1',
            'BPM.S.5L8.B1', 'BPM.E.5L8.B1',
            'BPMYB.S.4L8.B1', 'BPMYB.E.4L8.B1',
            'VSSG.S.4L8.B1', 'VSSG.E.4L8.B1',
            'VSSJ.S.4L8.B1', 'VSSJ.E.4L8.B1',
            'BPMSX.S.4L8.B1', 'BPMSX.E.4L8.B1',
            'VSSK.S.4L8.B1', 'VSSK.E.4L8.B1',
            'VSSG.S.B3L8.B1', 'VSSG.E.B3L8.B1',
            'VSSG.S.A3L8.B1', 'VSSG.E.A3L8.B1',
            'BPMS.S.2L8.B1', 'BPMS.E.2L8.B1',
            'VSSL.S.2L8.B1', 'VSSL.E.2L8.B1',
            'BPMSW.S.1L8.B1', 'BPMSW.E.1L8.B1',
            'BPMSW.S.1R8.B1', 'BPMSW.E.1R8.B1',
            'VSSL.S.1R8.B1', 'VSSL.E.1R8.B1',
            'BPMS.S.2R8.B1', 'BPMS.E.2R8.B1',
            'VSSG.S.2R8.B1', 'VSSG.E.2R8.B1',
            'VSSG.S.3R8.B1', 'VSSG.E.3R8.B1',
            'VSSK.S.4R8.B1', 'VSSK.E.4R8.B1',
            'BPMSX.S.4R8.B1', 'BPMSX.E.4R8.B1',
            'BPMWB.S.4R8.B1', 'BPMWB.E.4R8.B1',
            'VSSJ.S.4R8.B1', 'VSSJ.E.4R8.B1',
            'VSSG.S.4R8.B1', 'VSSG.E.4R8.B1',
            'BPMYB.S.4R8.B1', 'BPMYB.E.4R8.B1',
            'BPMYB.S.5R8.B1', 'BPMYB.E.5R8.B1',
            'VSSG.S.5R8.B1', 'VSSG.E.5R8.B1',
            'VSSB.S.6R8.B1', 'VSSB.E.6R8.B1',
            'BPM.S.6R8.B1', 'BPM.E.6R8.B1',
            'BPM_A.S.7R8.B1', 'BPM_A.E.7R8.B1',
            'VSSB.S.7R8.B1', 'VSSB.E.7R8.B1',  
            ]

    else:
        # look for aperture markers in this range
        #             from           to
        eleNames = [ "E.DS.L1.B2" , "MK.IP8.R.27", 
                     "MK.IP8.L.28", "S.DS.R5.B2" ,
                     "E.DS.L5.B2" , "MK.IP2.R.26",
                     "MK.IP2.L.29", "S.DS.R1.B2" ]
        # look for aperture markers assigned to drifts in this range
        #    from          to
        eleNamesDrift = [
            ]
        # do not dump these elements
        escapeFromDumping = [ \
            "LHCINJ.B2", "IR2$START", "IR2$END" 
            ]
        # dump elements but commented, since, if they are inserted, 
        #      MADX will generate a negative drift error
        negativeDrift = [ \
            'VSSB.S.C11L2.B2', 'VSSB.E.C11L2.B2',
            'VSSB.S.C11R2.B2', 'VSSB.E.C11R2.B2',
            'VSSB.S.C11L3.B2', 'VSSB.E.C11L3.B2',
            'VSSB.S.C11R3.B2', 'VSSB.E.C11R3.B2',
            'VSSB.S.C11L4.B2', 'VSSB.E.C11L4.B2',
            'VSSB.S.C11R4.B2', 'VSSB.E.C11R4.B2',
            'VSSB.S.C11L6.B2', 'VSSB.E.C11L6.B2',
            'VSSB.S.C11R6.B2', 'VSSB.E.C11R6.B2',
            'VSSB.S.C11L7.B2', 'VSSB.E.C11L7.B2',
            'VSSB.S.C11R7.B2', 'VSSB.E.C11R7.B2',
            'VSSB.S.C11L8.B2', 'VSSB.E.C11L8.B2',
            'VSSB.S.C11R8.B2', 'VSSB.E.C11R8.B2',
            'VSSG.S.7L2.B2'  , 'VSSG.E.7L2.B2'  ,
            'VSSK.S.3L2.B2'  , 'VSSK.E.3L2.B2'  ,
            'VSSK.S.3R2.B2'  , 'VSSK.E.3R2.B2'  ,
            'VSSG.S.7R2.B2'  , 'VSSG.E.7R2.B2'  ,
            'VSSG.S.7L3.B2'  , 'VSSG.E.7L3.B2'  ,
            'VSSG.S.7R3.B2'  , 'VSSG.E.7R3.B2'  ,
            'VSSG.S.7L4.B2'  , 'VSSG.E.7L4.B2'  ,
            'VSSG.S.7R4.B2'  , 'VSSG.E.7R4.B2'  ,
            'VSSG.S.7L7.B2'  , 'VSSG.E.7L7.B2'  ,
            'VSSG.S.7R7.B2'  , 'VSSG.E.7R7.B2'  ,
            'VSSG.S.7L8.B2'  , 'VSSG.E.7L8.B2'  ,
            'VSSK.S.3L8.B2'  , 'VSSK.E.3L8.B2'  ,
            'VSSK.S.3R8.B2'  , 'VSSK.E.3R8.B2'  ,
            'VSSG.S.7R8.B2'  , 'VSSG.E.7R8.B2'  ,
            'BPMWE.S.5L3.B2' , 'BPMWE.E.5L3.B2' ,
            'VSSG.S.B5L6.B2' , 'VSSG.E.B5L6.B2' ,
            'VSSG.S.A5L6.B2' , 'VSSG.E.A5L6.B2' ,
            'VSSG.S.A5R6.B2' , 'VSSG.E.A5R6.B2' ,
            'VSSG.S.B5R6.B2' , 'VSSG.E.B5R6.B2' ,
            'BPMWB.E.4R8.B2' , 'BPMWB.S.4R8.B2'
            ]
        # dump elements but commented, since, if they are inserted, 
        #      MADX will complain due to their position
        incorrectPositioning = [ \
            'BPMWE.S.5R3.B2' , 'BPMWE.E.5R3.B2' ,
            'BPMYB.S.5L6.B2' , 'BPMYB.E.5L6.B2' ,
            'VSSG.S.6L4.B2'  , 'VSSG.E.6L4.B2'  ,
            'VSSG.S.6R4.B2'  , 'VSSG.E.6R4.B2'  ,
            'BPMSD.S.4L6.B2' , 'BPMSD.E.4L6.B2' ,
            'BPMSB.S.B4L6.B2', 'BPMSB.E.B4L6.B2',
            'BPMSB.S.A4L6.B2', 'BPMSB.E.A4L6.B2' 
            ]
        # dump elements but commented, since they are available in layoutDB
        duplicateElements = [ \
            'VSSB.E.7R8.B2' , 'VSSB.S.7R8.B2' ,        
            'BPM_A.E.7R8.B2' , 'BPM_A.S.7R8.B2' ,        
            'BPMR.E.6R8.B2' , 'BPMR.S.6R8.B2' ,         
            'VSSB.E.6R8.B2' , 'VSSB.S.6R8.B2' ,         
            'VSSG.E.5R8.B2' , 'VSSG.S.5R8.B2' ,         
            'BPMYB.E.5R8.B2' , 'BPMYB.S.5R8.B2' ,        
            'BPMYB.E.4R8.B2' , 'BPMYB.S.4R8.B2' ,        
            'VSSG.E.4R8.B2' , 'VSSG.S.4R8.B2' ,         
            'VSSJ.E.4R8.B2' , 'VSSJ.S.4R8.B2' ,         
            'BPMSX.E.4R8.B2' , 'BPMSX.S.4R8.B2' ,        
            'VSSK.E.4R8.B2' , 'VSSK.S.4R8.B2' ,         
            'VSSG.E.3R8.B2' , 'VSSG.S.3R8.B2' ,         
            'VSSG.E.2R8.B2' , 'VSSG.S.2R8.B2' ,         
            'BPMS.E.2R8.B2' , 'BPMS.S.2R8.B2' ,         
            'VSSL.E.1R8.B2' , 'VSSL.S.1R8.B2' ,         
            'BPMSW.E.1R8.B2' , 'BPMSW.S.1R8.B2' ,        
            'BPMSW.E.1L8.B2' , 'BPMSW.S.1L8.B2' ,        
            'VSSL.E.2L8.B2' , 'VSSL.S.2L8.B2' ,         
            'BPMS.E.2L8.B2' , 'BPMS.S.2L8.B2' ,         
            'VSSG.E.A3L8.B2' , 'VSSG.S.A3L8.B2' ,        
            'VSSG.E.B3L8.B2' , 'VSSG.S.B3L8.B2' ,        
            'VSSK.E.4L8.B2' , 'VSSK.S.4L8.B2' ,         
            'BPMSX.E.4L8.B2' , 'BPMSX.S.4L8.B2' ,        
            'BPMWB.E.4L8.B2' , 'BPMWB.S.4L8.B2' ,        
            'VSSJ.E.4L8.B2' , 'VSSJ.S.4L8.B2' ,         
            'VSSG.E.4L8.B2' , 'VSSG.S.4L8.B2' ,         
            'BPMYB.E.4L8.B2' , 'BPMYB.S.4L8.B2' ,        
            'BPMR.E.5L8.B2' , 'BPMR.S.5L8.B2' ,         
            'VSSB.E.5L8.B2' , 'VSSB.S.5L8.B2' ,         
            'BPM.E.6L8.B2' , 'BPM.S.6L8.B2' ,          
            'VSSB.E.6L8.B2' , 'VSSB.S.6L8.B2' ,         
            'VSSB.E.7L8.B2' , 'VSSB.S.7L8.B2' ,         
            'BPM.E.7L8.B2' , 'BPM.S.7L8.B2' ,           
            'VSSB.E.7R7.B2' , 'VSSB.S.7R7.B2' ,         
            'BPM_A.E.7R7.B2' , 'BPM_A.S.7R7.B2' ,        
            'VSSB.E.6R7.B2' , 'VSSB.S.6R7.B2' ,         
            'BPM.E.6R7.B2' , 'BPM.S.6R7.B2' ,          
            'BPMWE.E.5R7.B2' , 'BPMWE.S.5R7.B2' ,        
            'BPMW.E.5R7.B2' , 'BPMW.S.5R7.B2' ,         
            'BPMWE.E.4R7.B2' , 'BPMWE.S.4R7.B2' ,        
            'BPMW.E.4R7.B2' , 'BPMW.S.4R7.B2' ,         
            'BPMW.E.4L7.B2' , 'BPMW.S.4L7.B2' ,         
            'BPMWE.E.4L7.B2' , 'BPMWE.S.4L7.B2' ,        
            'BPMW.E.5L7.B2' , 'BPMW.S.5L7.B2' ,         
            'BPMWE.E.5L7.B2' , 'BPMWE.S.5L7.B2' ,        
            'BPMWC.E.6L7.B2' , 'BPMWC.S.6L7.B2' ,        
            'VSSB.E.6L7.B2' , 'VSSB.S.6L7.B2' ,         
            'BPMR.E.6L7.B2' , 'BPMR.S.6L7.B2' ,         
            'VSSB.E.7L7.B2' , 'VSSB.S.7L7.B2' ,         
            'BPM.E.7L7.B2' , 'BPM.S.7L7.B2' ,             
            'BPMYA.E.5R6.B2' , 'BPMYA.S.5R6.B2' ,        
            'VSSG.E.4R6.B2' , 'VSSG.S.4R6.B2' ,         
            'BPMYB.E.4R6.B2' , 'BPMYB.S.4R6.B2' ,        
            'BPMSA.E.B4R6.B2' , 'BPMSA.S.B4R6.B2' ,       
            'BPMSA.E.A4R6.B2' , 'BPMSA.S.A4R6.B2' ,       
            'BPMSE.E.4R6.B2' , 'BPMSE.S.4R6.B2' ,        
            'VSSG.E.4L6.B2' , 'VSSG.S.4L6.B2' ,         
            'BPMYA.E.4L6.B2' , 'BPMYA.S.4L6.B2' ,        
            'VSSB.E.7R4.B2' , 'VSSB.S.7R4.B2' ,         
            'BPMCA.E.7R4.B2' , 'BPMCA.S.7R4.B2' ,        
            'BPMYB.E.6R4.B2' , 'BPMYB.S.6R4.B2' ,        
            'BPMYA.E.5R4.B2' , 'BPMYA.S.5R4.B2' ,        
            'VSSG.E.5R4.B2' , 'VSSG.S.5R4.B2' ,         
            'VSSJ.E.B5R4.B2' , 'VSSJ.S.B5R4.B2' ,        
            'VSSJ.E.A5R4.B2' , 'VSSJ.S.A5R4.B2' ,        
            'BPMWA.E.B5R4.B2' , 'BPMWA.S.B5R4.B2' ,       
            'BPMWA.E.A5R4.B2' , 'BPMWA.S.A5R4.B2' ,       
            'BPMWA.E.A5L4.B2' , 'BPMWA.S.A5L4.B2' ,       
            'BPMWA.E.B5L4.B2' , 'BPMWA.S.B5L4.B2' ,       
            'VSSJ.E.A5L4.B2' , 'VSSJ.S.A5L4.B2' ,        
            'VSSJ.E.B5L4.B2' , 'VSSJ.S.B5L4.B2' ,        
            'VSSG.E.5L4.B2' , 'VSSG.S.5L4.B2' ,         
            'BPMYB.E.5L4.B2' , 'BPMYB.S.5L4.B2' ,        
            'BPMYA.E.6L4.B2' , 'BPMYA.S.6L4.B2' ,        
            'VSSB.E.7L4.B2' , 'VSSB.S.7L4.B2' ,         
            'BPMC.E.7L4.B2' , 'BPMC.S.7L4.B2' ,            
            'VSSB.E.7R3.B2' , 'VSSB.S.7R3.B2' ,         
            'BPM_A.E.7R3.B2' , 'BPM_A.S.7R3.B2' ,        
            'BPM.E.6R3.B2' , 'BPM.S.6R3.B2' ,          
            'VSSB.E.6R3.B2' , 'VSSB.S.6R3.B2' ,         
            'BPMWC.E.6R3.B2' , 'BPMWC.S.6R3.B2' ,        
            'BPMW.E.5R3.B2' , 'BPMW.S.5R3.B2' ,         
            'BPMWE.E.4R3.B2' , 'BPMWE.S.4R3.B2' ,        
            'BPMW.E.4R3.B2' , 'BPMW.S.4R3.B2' ,         
            'BPMW.E.4L3.B2' , 'BPMW.S.4L3.B2' ,         
            'BPMWE.E.4L3.B2' , 'BPMWE.S.4L3.B2' ,        
            'BPMW.E.5L3.B2' , 'BPMW.S.5L3.B2' ,         
            'BPMR.E.6L3.B2' , 'BPMR.S.6L3.B2' ,         
            'VSSB.E.6L3.B2' , 'VSSB.S.6L3.B2' ,         
            'VSSB.E.7L3.B2' , 'VSSB.S.7L3.B2' ,         
            'BPM.E.7L3.B2' , 'BPM.S.7L3.B2' ,              
            'VSSB.E.7R2.B2' , 'VSSB.S.7R2.B2' ,         
            'BPM_A.E.7R2.B2' , 'BPM_A.S.7R2.B2' ,        
            'BPMR.E.6R2.B2' , 'BPMR.S.6R2.B2' ,         
            'VSSB.E.6R2.B2' , 'VSSB.S.6R2.B2' ,         
            'BPM.E.5R2.B2' , 'BPM.S.5R2.B2' ,          
            'VSSB.E.5R2.B2' , 'VSSB.S.5R2.B2' ,         
            'BPMYB.E.4R2.B2' , 'BPMYB.S.4R2.B2' ,        
            'VSSG.E.4R2.B2' , 'VSSG.S.4R2.B2' ,         
            'VSSJ.E.4R2.B2' , 'VSSJ.S.4R2.B2' ,         
            'BPMWB.E.4R2.B2' , 'BPMWB.S.4R2.B2' ,        
            'BPMSX.E.4R2.B2' , 'BPMSX.S.4R2.B2' ,        
            'VSSK.E.4R2.B2' , 'VSSK.S.4R2.B2' ,         
            'VSSG.E.3R2.B2' , 'VSSG.S.3R2.B2' ,         
            'VSSG.E.2R2.B2' , 'VSSG.S.2R2.B2' ,         
            'BPMS.E.2R2.B2' , 'BPMS.S.2R2.B2' ,         
            'VSSL.E.1R2.B2' , 'VSSL.S.1R2.B2' ,         
            'BPMSW.E.1R2.B2' , 'BPMSW.S.1R2.B2' ,        
            'BPMSW.E.1L2.B2' , 'BPMSW.S.1L2.B2' ,        
            'VSSL.E.2L2.B2' , 'VSSL.S.2L2.B2' ,         
            'BPMS.E.2L2.B2' , 'BPMS.S.2L2.B2' ,         
            'VSSG.E.A3L2.B2' , 'VSSG.S.A3L2.B2' ,        
            'VSSG.E.B3L2.B2' , 'VSSG.S.B3L2.B2' ,        
            'VSSK.E.4L2.B2' , 'VSSK.S.4L2.B2' ,         
            'BPMSX.E.4L2.B2' , 'BPMSX.S.4L2.B2' ,        
            'BPMWB.E.4L2.B2' , 'BPMWB.S.4L2.B2' ,        
            'VSSJ.E.4L2.B2' , 'VSSJ.S.4L2.B2' ,         
            'VSSG.E.4L2.B2' , 'VSSG.S.4L2.B2' ,         
            'BPMYB.E.4L2.B2' , 'BPMYB.S.4L2.B2' ,        
            'BPMYB.E.5L2.B2' , 'BPMYB.S.5L2.B2' ,        
            'VSSG.E.5L2.B2' , 'VSSG.S.5L2.B2' ,         
            'BPM.E.6L2.B2' , 'BPM.S.6L2.B2' ,          
            'VSSB.E.6L2.B2' , 'VSSB.S.6L2.B2' ,         
            'VSSB.E.7L2.B2' , 'VSSB.S.7L2.B2' ,         
            'BPM.E.7L2.B2' , 'BPM.S.7L2.B2' ,           
            ]

    # --------------------------------------------------------------------------
    # open output files
    try:
        oFileDef = open( oFileNameDef, 'w' )
    except:
        print(' unable to open file %s - aborting...' % ( oFileNameDef ))
        sys.exit()
    try:
        oFileIns = open( oFileNameIns, 'w' )
    except:
        print(' unable to open file %s - aborting...' % ( oFileNameIns ))
        sys.exit()
    oFileIns.write( 'SEQEDIT, sequence=lhcb%i; \n' % ( idBeam ) )

    # --------------------------------------------------------------------------
    # read file
    try:
        iFile = open( iFileName, 'r' )
    except:
        print(' unable to open file %s - aborting...' % ( iFileName ))
        sys.exit()
    lines = iFile.readlines()
    iFile.close()

    # --------------------------------------------------------------------------
    # parse lines
    lConsider = False
    lConsiderDrift = False
    iUnique=0
    for line in lines:
        data = line.split()
        name = data[1].strip('"')
    
        # an element of interest:
        if ( name in eleNames ):
            lConsider = not lConsider
            print(' ...found element of interest %s - lConsider switched to %s' % \
                ( name, lConsider ))
            continue
        if ( name in eleNamesDrift ):
            lConsiderDrift = not lConsiderDrift
            print(' ...found element of interest %s - lConsiderDrift switched to %s' % \
                ( name, lConsiderDrift ))
            continue
        if ( data[0] == '"MARKER"' ):
            if ( lConsider ):
                if ( not name in escapeFromDumping and
                     not "0.000000" in data[5:9]   and
                     ( ( not lConsiderDrift and not 'DRIFT' in name ) or
                     ( (     lConsiderDrift and     'DRIFT' in name  ) ) ) ):
                    if ( name in negativeDrift or name in incorrectPositioning ):
                        headChar = '! '
                    elif (name in duplicateElements):
                        headChar = '! duplicate'
                    else:
                        headChar = ''
                    sPos = float( data[3] )
                    if ( idBeam==2 and not '.B2' in name ):
                        # non-unique marker
                        iUnique=iUnique+1
                        name = name+ '.%03i' % (iUnique) +'.B2'
                        sPos = sPos -0.002
                    if ( idBeam==2 and not isB4 ):
                        sPos = AccelLen-data[3]
                    sPos = "%.6f" % ( sPos )
                    oFileDef.write( MARKERlineFmt1 
                                    % ( headChar, name, data[5], data[6], data[7], data[8] ) )
                    oFileIns.write( MARKERlineFmt2 % ( headChar, name, sPos ) )

    # --------------------------------------------------------------------------
    # fuck off you all!!
    oFileIns.write( 'ENDEDIT; \n' )
    oFileIns.write( 'return; \n' )
    oFileDef.write( 'return; \n' )
    oFileIns.close()
    oFileDef.close()
    print(' ...done.')
