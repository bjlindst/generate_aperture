
input=patchesFiles/layoutDB_negDrifts.txt
while IFS= read -r line
do
    echo "Commenting negDrift: ""$line"
    sed -i "s/install, element = $line/!negDrift install, element = $line/g" processedFiles/layoutDB.seq
done < "$input"

input=patchesFiles/patches.txt
while IFS= read -r line
do
    echo "Commenting patches: ""$line"
    sed -i "s/install, element = $line/!inconsistent install, element = $line/g" processedFiles/layoutDB.seq
done < "$input"     

input=patchesFiles/shiftLeft.txt
while IFS= read -r line
do
    echo "Shifting left: ""$line"
    sed -i "s/install, element = $line, at=/install, element = $line, at= -0.001 +/g" processedFiles/layoutDB.seq
done < "$input"   

input=patchesFiles/shiftRight.txt
while IFS= read -r line
do
    echo "Shifting right: ""$line"
    sed -i "s/install, element = $line, at=/install, element = $line, at= +0.001 +/g" processedFiles/layoutDB.seq
done < "$input"     

input=patchesFiles/shiftLeft_b4.txt # this is for b2, so left means positive
while IFS= read -r line
do
    echo "Shifting left: ""$line"
    sed -i "s/install, element = $line, at=/install, element = $line, at= +0.001 +/g" processedFiles/layoutDB.seq
done < "$input"   

input=patchesFiles/shiftRight_b4.txt # this is for b2, so right means negative
while IFS= read -r line
do
    echo "Shifting right: ""$line"
    sed -i "s/install, element = $line, at=/install, element = $line, at= -0.001 +/g" processedFiles/layoutDB.seq
done < "$input"    


# BPMYB.5L2.B1 at wrong location according to layoutDB, causing a spike, adjusting the beam screen in layoutDB to remove spike
echo "Shifting VSSG.5L2.B.B1"
sed -i "s/install, element = VSSG.5L2.B.B1, at=/install, element = VSSG.5L2.B.B1, at= -0.01 +/g" processedFiles/layoutDB.seq  

# BPMYB.5R6.B1 at wrong location according to layoutDB, causing a spike, adjusting the beam screen in layoutDB to remove spike
echo "Shifting VSSG.5R6.A.B1"
sed -i "s/install, element = VSSG.5R6.A.B1, at=/install, element = VSSG.5R6.A.B1, at= +0.02 +/g" processedFiles/layoutDB.seq  

# BPMYB.6L4.B1 at wrong location according to layoutDB, causing a spike, adjusting the beam screen in layoutDB to remove spike
echo "Shifting VSSG.6L4.A.B1"
sed -i "s/install, element = VSSG.6L4.A.B1, at=/install, element = VSSG.6L4.A.B1, at= +0.02 +/g" processedFiles/layoutDB.seq   

# marker causes a spike with VSSG.7R4.B.B1.
echo "Shifting BPMCS.7R4.A.B1"
sed -i "s/install, element = BPMCS.7R4.A.B1, at=/install, element = BPMCS.7R4.A.B1, at= +0.01 +/g" processedFiles/layoutDB.seq   

# marker causes a spike with two VVGST
echo "Shifting HCVSSB_345.5L1.A.B1"
sed -i "s/install, element = HCVSSB_345.5L1.A.B1, at=/install, element = HCVSSB_345.5L1.A.B1, at= +0.2 +/g" processedFiles/layoutDB.seq     

# BPMYB.4R2.B1 location not synchronized with layoutDB, fix by moving VSSG.4R2.B.B1 left by 0.01
echo "Shifting VSSG.4R2.B.B1"
sed -i "s/install, element = VSSG.4R2.B.B1, at=/install, element = VSSG.4R2.B.B1, at= -0.01 +/g" processedFiles/layoutDB.seq  

# VSSG.4R8.B.B1 to left by 0.01 
echo "Shifting VSSG.4R8.B.B1"
sed -i "s/install, element = VSSG.4R8.B.B1, at=/install, element = VSSG.4R8.B.B1, at= -0.01 +/g" processedFiles/layoutDB.seq    

# VSSG.5L6.B.B1 move left by 0.2 - ends of IR6 not synchronized properly, quick fix as follows... 
echo "Shifting VSSG.5L6.B.B1"
sed -i "s/install, element = VSSG.5L6.B.B1, at=/install, element = VSSG.5L6.B.B1, at= -0.2 +/g" processedFiles/layoutDB.seq  

# VSSG.5R6.D.B1 move left by 0.3 - ends of IR6 not synchronized properly, quick fix as follows... 
echo "Shifting VSSG.5R6.D.B1"
sed -i "s/install, element = VSSG.5R6.D.B1, at=/install, element = VSSG.5R6.D.B1, at= -0.3 +/g" processedFiles/layoutDB.seq  

# VSSG.4L6.A.B1 move right by 0.01   
echo "Shifting VSSG.4L6.A.B1"
sed -i "s/install, element = VSSG.4L6.A.B1, at=/install, element = VSSG.4L6.A.B1, at= +0.01 +/g" processedFiles/layoutDB.seq 

# VCDUB.5L4.B.B1 move left by > 0.017 m 
echo "Shifting VCDUB.5L4.B.B1"
sed -i "s/install, element = VCDUB.5L4.B.B1, at=/install, element = VCDUB.5L4.B.B1, at= -0.0171 +/g" processedFiles/layoutDB.seq 

# FIX negative drifts of elements that are required for good aperture #
# negative drift between elements  vctcw.6l2.b.b1:1 and msib.c6l2.b1:1, length -1.183622e-03   
echo "Shifting VCTCW.6L2.B.B1"
sed -i "s/install, element = VCTCW.6L2.B.B1, at=/install, element = VCTCW.6L2.B.B1, at= -0.002 +/g" processedFiles/layoutDB.seq  
# negative drift between elements  vcsig.6l2.a.b1:1 and msib.c6l2.b1:1, length -1.183622e-03   
echo "Shifting VCSIG.6L2.A.B1"
sed -i "s/install, element = VCSIG.6L2.A.B1, at=/install, element = VCSIG.6L2.A.B1, at= -0.0021 +/g" processedFiles/layoutDB.seq   
# negative drift between elements  vssb.5l6.a.b1:1 and lejl.5l6.b1:1, length -5.542264e-01
echo "Shifting VSSB.5L6.A.B1"
sed -i "s/install, element = VSSB.5L6.A.B1, at=/install, element = VSSB.5L6.A.B1, at= -0.56 +/g" processedFiles/layoutDB.seq   
# negative drift between elements  lejl.5l6.b1:1 and vssb.5l6.b.b1:1, length -1.305736e-01
echo "Shifting VSSB.5L6.B.B1"
sed -i "s/install, element = VSSB.5L6.B.B1, at=/install, element = VSSB.5L6.B.B1, at= +0.131 +/g" processedFiles/layoutDB.seq  
# negative drift between elements  vamfn.6r8.b.b1:1 and msia.a6r8.b1:1, length -6.889230e-04
echo "Shifting VAMFN.6R8.B.B1"
sed -i "s/install, element = VAMFN.6R8.B.B1, at=/install, element = VAMFN.6R8.B.B1, at= -0.001 +/g" processedFiles/layoutDB.seq  
# negative drift between elements  vcsim.6r8.a.b1:1 and msia.a6r8.b1:1, length -6.889230e-04
echo "Shifting VCSIM.6R8.A.B1"
sed -i "s/install, element = VCSIM.6R8.A.B1, at=/install, element = VCSIM.6R8.A.B1, at= -0.001 +/g" processedFiles/layoutDB.seq  
# negative drift between elements  vssb.5l6.a.b2:1 and lejl.5l6.b2:1, length -5.542264e-01
echo "Shifting VSSB.5L6.A.B2"
sed -i "s/install, element = VSSB.5L6.A.B2, at=/install, element = VSSB.5L6.A.B2, at= -0.56 +/g" processedFiles/layoutDB.seq     
# negative drift between elements  lejl.5l6.b2:1 and vssb.5l6.b.b2:1, length -1.305736e-01
echo "Shifting VSSB.5L6.B.B2"
sed -i "s/install, element = VSSB.5L6.B.B2, at=/install, element = VSSB.5L6.B.B2, at= +0.131 +/g" processedFiles/layoutDB.seq  

echo "FIX aperture definitions"
sed -i 's/AP079: marker, apertype= RECTELLIPSE, aperture= {0.03, 0.04, 0.04, 0.04};/AP079: marker, apertype= RECTELLIPSE, aperture= {0.04, 0.04, 0.04, 0.04};/g' processedFiles/layoutDB.seq
sed -i 's/AP180: marker, apertype= RECTELLIPSE, aperture= {0.02121, 0.02121, 0.02828, 0.02828};/AP180: marker, apertype= RECTELLIPSE, aperture= {0.04, 0.04, 0.04, 0.04};/g' processedFiles/layoutDB.seq


echo "Correcting signs"
sed -i "s/+ +/+/g" processedFiles/layoutDB.seq
sed -i "s/+ -/-/g" processedFiles/layoutDB.seq

echo "Fix bad names for MKDCAs"
sed -i 's/MKDCA.5R6.\[/MKDCA.5R6.AA/g' processedFiles/layoutDB.seq
sed -i 's/MKDCA.5R6.\\/MKDCA.5R6.BB/g' processedFiles/layoutDB.seq
sed -i 's/MKDCA.5R6.\]/MKDCA.5R6.CC/g' processedFiles/layoutDB.seq
sed -i 's/MKDCA.5R6.\^/MKDCA.5R6.DD/g' processedFiles/layoutDB.seq

sed -i 's/MKDCA.5L6.\[/MKDCA.5L6.AA/g' processedFiles/layoutDB.seq
sed -i 's/MKDCA.5L6.\\/MKDCA.5L6.BB/g' processedFiles/layoutDB.seq
sed -i 's/MKDCA.5L6.\]/MKDCA.5L6.CC/g' processedFiles/layoutDB.seq
sed -i 's/MKDCA.5L6.\^/MKDCA.5L6.DD/g' processedFiles/layoutDB.seq 


echo "Some sanity checks:"
sanity1=$(grep negDrift processedFiles/layoutDB.seq | head)
sanity2=$(grep inconsistent processedFiles/layoutDB.seq | head)
echo $sanity1
echo $sanity2

echo "DONE"  


