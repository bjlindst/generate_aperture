##############################################################################################
#
#  Script for generating the necessary input files to get a good aperture in MADX
#  Run the script without arguments in the terminal. The input files for MADX will be
#  Generated in the folder processedFiles/
#
##############################################################################################

python extraTools/parseExistingApertureModelThick.py originalFiles/allapert.b2 2
mv fromCollimation_define.madx processedFiles/fromCollimation_define_b4.madx
mv fromCollimation_install.madx processedFiles/fromCollimation_install_b4.madx
cp originalFiles/manualPatches/correctMechSep3_b4.madx processedFiles/
cp originalFiles/manualPatches/patchApertures_b4_release_v06.madx processedFiles/

rm processedFiles/layoutDB_b4_part1.seq
rm processedFiles/layoutDB_b4_part2.seq
cp originalFiles/APERTURE_LHC_LS3_28-JUL-2021_b4_part1.seq processedFiles/layoutDB_b4_part1.seq
cp originalFiles/APERTURE_LHC_LS3_28-JUL-2021_b4_part2.seq processedFiles/layoutDB_b4_part2.seq   
echo "Running sedScript_b4"
./sedScript_b4.sh 

# VMIAL aperture
sed -i "s/AP038: marker, apertype= RECTELLIPSE, aperture= {0.08, 0.08, 0.08, 0.08};/AP038: marker, apertype= RECTELLIPSE, aperture= {0.04, 0.04, 0.04, 0.04};/g" processedFiles/layoutDB_b4_part1.seq
# VCDSS aperture
sed -i "s/AP199: marker, apertype= RECTELLIPSE, aperture= {0., 0., 0.04, 0.04};/AP199: marker, apertype= RECTELLIPSE, aperture= {0.04, 0.04, 0.04, 0.04};/g" processedFiles/layoutDB_b4_part1.seq   

### 20210827
# VVGSW aperture is 100 mm diameter
sed -i "s/VVGSW.4R6.A.B1: AP192/VVGSW.4R6.A.B1: AP010/g" processedFiles/layoutDB_b4_part1.seq
sed -i "s/VVGSW.4R6.B.B1: AP192/VVGSW.4R6.B.B1: AP010/g" processedFiles/layoutDB_b4_part1.seq 
sed -i "s/VVGSW.4L6.A.B2: AP192/VVGSW.4L6.A.B2: AP010/g" processedFiles/layoutDB_b4_part1.seq 
sed -i "s/VVGSW.4L6.B.B2: AP192/VVGSW.4L6.B.B2: AP010/g" processedFiles/layoutDB_b4_part1.seq  

### flip sign of mech_sep

sed '/B2/s/mech_sep= /mech_sep= -/' -i processedFiles/layoutDB_b4_part1.seq
sed '/B2/s/--//' -i processedFiles/layoutDB_b4_part1.seq
sed '/B2/s/-0,/0,/' -i processedFiles/layoutDB_b4_part1.seq



