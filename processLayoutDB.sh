##############################################################################################
#
#  Script for generating the necessary input files to get a good aperture in MADX
#  Run the script without arguments in the terminal. The input files for MADX will be
#  Generated in the folder processedFiles/
#
############################################################################################## 



python extraTools/parseExistingApertureModelThick.py originalFiles/allapert.b1 1
mv fromCollimation_define.madx processedFiles/
mv fromCollimation_install.madx processedFiles/
cp originalFiles/manualPatches/correctMechSep3.madx processedFiles/
cp originalFiles/manualPatches/patchApertures_b1_release_v06.madx processedFiles/

rm processedFiles/layoutDB.seq
cp originalFiles/APERTURE_LHC_LS3_28-JUL-2021.seq processedFiles/layoutDB.seq

echo "Running sedScript"
./sedScript.sh 

# VMIAL aperture
sed -i "s/AP038: marker, apertype= RECTELLIPSE, aperture= {0.08, 0.08, 0.08, 0.08};/AP038: marker, apertype= RECTELLIPSE, aperture= {0.04, 0.04, 0.04, 0.04};/g" processedFiles/layoutDB.seq
# VCDSS aperture
sed -i "s/AP199: marker, apertype= RECTELLIPSE, aperture= {0., 0., 0.04, 0.04};/AP199: marker, apertype= RECTELLIPSE, aperture= {0.04, 0.04, 0.04, 0.04};/g" processedFiles/layoutDB.seq   

### 20210827
# VVGSW aperture diameter should be 100 mm
sed -i "s/VVGSW.4R6.A.B1: AP192/VVGSW.4R6.A.B1: AP010/g" processedFiles/layoutDB.seq
sed -i "s/VVGSW.4R6.B.B1: AP192/VVGSW.4R6.B.B1: AP010/g" processedFiles/layoutDB.seq 
sed -i "s/VVGSW.4L6.A.B2: AP192/VVGSW.4L6.A.B2: AP010/g" processedFiles/layoutDB.seq 
sed -i "s/VVGSW.4L6.B.B2: AP192/VVGSW.4L6.B.B2: AP010/g" processedFiles/layoutDB.seq    
